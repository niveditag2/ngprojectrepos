package com.trigrams;

import com.google.common.base.Joiner;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Trigram {


    public static String read(InputStream is) throws IOException {
        BufferedReader inputStream = new BufferedReader(new InputStreamReader(is));
        StringBuilder builder = new StringBuilder();
        String record = inputStream.readLine();
        while (record != null) {
            builder.append(record + "\n");
            record = inputStream.readLine();
        }
        return builder.toString();
    }

    private static void closeStream(InputStream is) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
                throw new RuntimeException("Unable to close file...", e);
            }
        }
    }
    public void generate(InputStream is)
    {
        try {
            KeyStore store = new KeyStore();
            BufferedReader inputStream = new BufferedReader(new InputStreamReader(is));
            StringBuilder builder = new StringBuilder();
            String record = inputStream.readLine();
            while (record != null) {
                builder.append(record + "\n");
                record = inputStream.readLine();
            }

            String text = builder.toString();
            store.addKeys(text);

            System.out.println("Original Text========");
            System.out.println(text);

            System.out.println("Trigram Text=======");
            System.out.println(Joiner.on("\n").join(wrapText(new Generator(store).generateText(), 80)));

        } catch (FileNotFoundException e) {
            throw new RuntimeException("File not found...", e);
        } catch (IOException e) {
            throw new RuntimeException("Keys could not be extracted...", e);
        }

    }

    private List<String> wrapText(String text, int len) {
        if (text == null) {
            return Collections.emptyList();
        }

        if (len <= 0 || text.length() <= len) {
            return Arrays.asList(text);
        }

        char[] chars = text.toCharArray();
        List<String> lines = new ArrayList<String>();
        StringBuffer line = new StringBuffer();
        StringBuffer word = new StringBuffer();

        for (int i = 0; i < chars.length; i++) {
            word.append(chars[i]);
            if (chars[i] == ' ') {
                if ((line.length() + word.length()) > len) {
                    lines.add(line.toString());
                    line.delete(0, line.length());
                }
                line.append(word);
                word.delete(0, word.length());
            }
        }

        if (word.length() > 0) {
            if ((line.length() + word.length()) > len) {
                lines.add(line.toString());
                line.delete(0, line.length());
            }
            line.append(word);
        }

        if (line.length() > 0) {
            lines.add(line.toString());
        }

        return lines;
    }

    public static void main(String args[]) throws FileNotFoundException {

        ClassLoader classLoader = Trigram.class.getClassLoader();
        File file = new File(classLoader.getResource("TestTrigram.txt").getFile());
        InputStream in = new FileInputStream(file);
        new Trigram().generate(in);
    }

}
