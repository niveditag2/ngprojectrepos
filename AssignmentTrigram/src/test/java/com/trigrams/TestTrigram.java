package com.trigrams;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class TestTrigram {

    @Test
    public void loadFileTest()
    {
        ClassLoader classLoader = getClass().getClassLoader();

        try (InputStream inputStream = classLoader.getResourceAsStream("TestTrigram.txt")) {

            String result = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
            System.out.println(result);

            new Trigram().generate(inputStream);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
